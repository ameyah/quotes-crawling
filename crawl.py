__author__ = 'Ameya'


import urllib2
import sys, getopt
from bs4 import BeautifulSoup
import html5lib
from HTMLParser import HTMLParser
import re


class NoRedirectHandler(urllib2.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        infourl = urllib2.addinfourl(fp, headers, req.get_full_url())
        infourl.status = code
        infourl.code = code
        return infourl
    http_error_300 = http_error_302
    http_error_301 = http_error_302
    http_error_303 = http_error_302
    http_error_307 = http_error_302


def processArguments(argv):
    global downloadUrl
    global output_file
    global tag_name
    global regular_expression_usage
    global class_name
    global id_name
    global link_pages
    try:
        opts, args = getopt.getopt(argv, "hi:o:t:rl:", ["ifile=", "ofile=", "tag=", "regex", "link"])
    except getopt.GetoptError:
        print('Usage: crawl.py -i <url> -o <outputFileName>')
        sys.exit(0)
    for opt, arg in opts:
        if opt == '-h':
            print('Usage: crawl.py -i <url> -o <outputFileName>')
            sys.exit(0)
        elif opt in ("-i", "--ifile"):
            downloadUrl = arg
            validationString = "http://"
            if downloadUrl.find(validationString) == -1:
                print("Invalid URL. Make sure the URL starts with a 'http://'")
                sys.exit(0)
        elif opt in ("-o", "--ofile"):
            output_file = arg
        elif opt in ("-t", "--tag"):
            tag_name = arg
        elif opt in ("-r", "-re"):
            regular_expression_usage = True
        elif opt in ("-l", "-link"):
            link_pages = arg


def get_data(downloadUrl):
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
            'Accept-Encoding': 'none',
            'Accept-Language': 'en-US,en;q=0.8',
            'Connection': 'keep-alive'}
        opener = urllib2.build_opener(NoRedirectHandler())
        urllib2.install_opener(opener)
        req = urllib2.Request(downloadUrl, headers=headers)
        response = urllib2.urlopen(req)
        html = response.read()
        response.close()
        return html
    except urllib2.HTTPError as e:
        print "Unable to read data from URL."
        sys.exit(0)


def get_next_page(original_url, link_site):
    if link_site.lower() == "brainyquote":
        get_next_page.counter += 1
        new_url = original_url.split(".html")[0] + str(get_next_page.counter) + ".html"
        print new_url
        return new_url


def is_valid_data(unformatted_data, link_site):
    if link_site.lower() == "brainyquote":
        if "ws-author" in unformatted_data:
            return False
        return True
    return True

# Static Variables
get_next_page.counter = 1

# Global variables
downloadUrl = ''
output_file = ''
tag_name = ''
regular_expression_usage = False
class_name = ''
id_name = ''
link_pages = ''

# Process Arguments
if len(sys.argv) == 1:
    print('Usage: crawl.py -i <url> -o <outputFileName>')
    sys.exit(0)
processArguments(sys.argv[1:])

html = get_data(downloadUrl)
if html == '':
    print "No data received."
    sys.exit(0)

if link_pages != '':
    while True:
        next_link = get_next_page(downloadUrl, link_pages)
        new_html = get_data(next_link)
        if new_html != '':
            html += new_html
        else:
            break

parsedHTML = BeautifulSoup(html, 'html5lib')
if "#" in tag_name:
    tag_split = tag_name.split("#")
    if regular_expression_usage:
        data = parsedHTML.find_all(tag_split[0], id_=re.compile(tag_split[1]))
    else:
        data = parsedHTML.find_all(tag_split[0], id_=tag_split[1])
elif "." in tag_name:
    tag_split = tag_name.split(".")
    if regular_expression_usage:
        data = parsedHTML.find_all(tag_split[0], class_=re.compile(tag_split[1]))
    else:
        data = parsedHTML.find_all(tag_split[0], class_=tag_split[1])
else:
    data = parsedHTML.find_all(tag_name)

output_file_handler = open(output_file, 'w')
output_file_handler.write(downloadUrl + "\n")
for item in data:
    if is_valid_data(str(item), link_pages):
        formatted_item = re.sub('<[^<]+?>', '', str(item))
        formatted_item = " ".join(re.findall("[a-zA-Z ]+", formatted_item))
        if formatted_item != '':
            output_file_handler.write("%s\n" % formatted_item)